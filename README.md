php-inc
=======

一个简单、易用的PHP开发包，提供常用的类库、函数库，方便集成到现有系统中使用。

## 示例项目

* [网址书签（单用户版）](https://bookmark.wuwenbin.info/doc/bookmark_s.md)

## 授权协议

* [MIT](http://opensource.org/licenses/MIT)

## 项目示例结构

```
/
/root/                          # 公共目录
/root/inc/                      # 公共库,模块等
/root/inc/php-inc/
/root/inc/model/
/root/inc/common.php
/root/inc/...
/root/tpl/                      # 模版
/root/tpl_c/                    # 模版缓存
/root/tpl_c/default/
/root/tpl_c/admin/
/root/log/                      # 日志
/root/cache/                    # 缓存
/root/init.php                  # 全局初始化
/root/config.php                # 全局配置
/root/...

/public/                        # 网站前台
/public/static/
/public/static/js/
/public/static/css/
/public/static/img/
/public/init.php
/public/index.php
/public/...

/admin/                         # 管理后台
/admin/static/
/admin/static/js/
/admin/static/css/
/admin/static/img/
/admin/init.php
/admin/index.php
/admin/...

/cmd/                           # 脚本工具
/init/                          # 初始化sql脚本
/backup/                        # 数据备份
/serv/                          # nginx配置
/...
```

## 常用第三方库

* [PHPMailer](https://github.com/PHPMailer/PHPMailer)：邮件收发
* [Captcha](https://github.com/Gregwar/Captcha): 图片验证码

## 联系方式

* <admin@wuwenbin.info>

